#ifndef DEMON_H
# define DEMON_H

#include<pcap.h>
#include<stdio.h>
#include<stdlib.h> // for exit()
#include<string.h> //for memset
#include <unistd.h> // fork
#include<sys/socket.h>
#include<arpa/inet.h> // for inet_ntoa()
#include<net/ethernet.h>
#include<netinet/ip_icmp.h>   //Provides declarations for icmp header
#include<netinet/udp.h>   //Provides declarations for udp header
#include<netinet/tcp.h>   //Provides declarations for tcp header
#include<netinet/ip.h>    //Provides declarations for ip header

#include <signal.h>
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <net/if.h>


#define FILE_INTERFACE "/var/log/interfaces.log"
#define PID_FILE "/var/log/pid_file.log"
#define ITERFACE_NAME "/var/log/interface_name.log"
#define FILE_LOG "/var/log/log_file.log"

int all;

struct sockaddr_in dest;

typedef struct  s_count_ip
{
  char **ip;
  int  count;
}               t_count_ip;

void demon(void);
void	signals_block(void);
void check_interface(void);
int file_set(void);
void set_pid(void);
void get_pid(void);
void seting_file(void);
void   snifer(void);
int corect_interface(char *name);
int set_interface_name(void);
void snifing(void);
void process_packet(u_char *args, const struct pcap_pkthdr *header, const u_char *buffer);
void save_packet(t_count_ip *count_ip, const u_char *buffer, int size);
int binarySearch(char **arr, int l, int r, char *x);
void	mergesort(void **tab, int length, int (*cmp)());
int check_doble_dot(char *tab);
char **add(char **arr, char *new, int len);
void		ft_freearr(char **arr);
void add_file(char **ip, int len);
void add_count(char *add);
char		*ft_argsjoin(char **args);

#endif
